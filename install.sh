#!/bin/sh

DATATAR="deepin.com.qq.im.light_7.9.14308deepin8_i386.deb"
FONTTAR="font.tar.xz"

echo "installing"

ar x $DATATAR
tar xf data.tar.xz
mkdir /app/data
mkdir /app/bin
mkdir /app/ext
cp ./opt/deepinwine/apps/Deepin-QQLight/files.7z /app/data/
cp ./$FONTTAR /app/data/
cp ./run.sh /app/bin/
chmod a+x /app/bin/run.sh

mkdir -p /app/share/applications /app/share/icons
cp ./com.tencent.qqlight.desktop /app/share/applications
cp ./com.tencent.qqlight.svg /app/share/icons

echo "done"
